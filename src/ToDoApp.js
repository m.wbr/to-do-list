import React, {Component} from 'react';
import './App.css';
import Header from './Header';
import ItemList from './ItemList';
import ItemCreationMask from './ItemCreationMask';

class ToDoApp extends Component {

  constructor(props){
    super(props)

    this.state = {
        items: [
            {
                description: 'MiroLab abschliessen',
                done: true
            },
            {
                description: 'Klausur auf Moodle hochladen',
                done: false
            },
            {
              description: 'Protokoll als erfolgreich eintragen: 017200872',
              done: false
            }
  
        ],
    }
    this.setItemToDone = this.setItemToDone.bind(this)
    this.setNewItem = this.setNewItem.bind(this)    
  }

  setItemToDone(finishedItem){
    
    this.setState((currentState) => {
      currentState = currentState.items.map((item) => {
        if(item.description === finishedItem.description){
          item.done = true
        }
        return item
      })
      return currentState
    })    
  }

  setNewItem(item){
    this.setState((currentState)=>{
      return currentState.items.push(item)
    })
  }

  render(){
    return(
      <div id='ToDoApp'>
        <Header text={'To Do List:'}/>
        <ItemList items={this.state.items} setItemToDone={this.setItemToDone}/>
        <ItemCreationMask setNewItem={this.setNewItem}/>
      </div>
    )
  }
}

export default ToDoApp;