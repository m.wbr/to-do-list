import React from 'react';

function Item(props){  

    const item = props.item
    const setItemToDone = props.setItemToDone
    const key = props.item.description

    if(item.done === true){
        return(
            <li className='done' key={key}>
                {item.description}
            </li>     
        )
    }else{
        return(
            <li key={key}>
                {item.description}
                <button className='button' onClick={()=>setItemToDone(item)}>Done!</button>
            </li>                     
        )
    }
}
export default Item;