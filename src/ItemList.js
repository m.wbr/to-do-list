import React from 'react'
import Item from './Item'

function ItemList(props){
    
    return (
        <div id='itemList'>
            <ul>                
                {props.items.map((item) => (                    
                    <Item item={item} setItemToDone={props.setItemToDone} />
                ))}
            </ul>
        </div>        
    )
}
export default ItemList;