import React, {Component} from 'react'

class ItemCreationMask extends Component{    

    constructor(props){
        super(props)

        this.state = {
            description: '',
            done: false   
        }        
        this.handleInputChange = this.handleInputChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }
    
    handleInputChange = (event) =>{
        event.preventDefault();

        this.setState({            
            description: event.target.value
        })
    }

    handleSubmit = (event) => {
        event.preventDefault();
        const data = this.state
        this.props.setNewItem(data)
        this.setState({
            description: ''
        })
    }
    
    render(){
        return (
            <div id='textField'>                
                <form onSubmit={this.handleSubmit}>
                    <input type="text" name="description" onChange={this.handleInputChange} value={this.state.description}/>
                    <button className='button'>Add</button>
                </form>                
            </div>
        )
    }
}
export default ItemCreationMask;